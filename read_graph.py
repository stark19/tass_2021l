# -*- coding: utf-8 -*-
import networkx as nx

class reader:
    def read_publications(self):
        publications_graph = nx.read_edgelist("siec_publikacji.txt",delimiter=";+",create_using=nx.MultiGraph)
        return publications_graph
    def read_patents(self):
        patents_graph = nx.read_edgelist("siec_patentow.txt",delimiter=";+",create_using=nx.MultiGraph)
        return patents_graph
