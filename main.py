from download_data import downloader
from create_graph import generator
from read_graph import reader
from analysis import analysis
from networkx.exception import NetworkXError

if __name__ == "__main__":
    #my_downloader = downloader(dump_data=True)
    #my_downloader.get_patents_data()
    #my_downloader.get_publications_data()
    #graph_generator = generator()
    #patents_graph = graph_generator.create_patents_graph()
    #publications_graph = graph_generator.create_publications_graph()
    graph_reader = reader()
    publications = graph_reader.read_publications()
    patents = graph_reader.read_patents()

#%%
analizator = analysis(patents,publications)
#Analiza kontaktów naukowców z sieci dot. wspólnego patentowania
#(bez uwzglednienia liczby publikacji)
result_1 = analizator.analysis_1()
#Analiza kontaktów naukowców z sieci dot. wspólnego patentowania (uwzględniając liczbę publikacji)
#(uwzgledniajac liczbe publikacji)
result_2 = analizator.analysis_2()
#Analiza kontaktów naukowców z sieci dot. wspólnego patentowania 
#(uwzględniając liczbę publikacji oraz czas zgłoszenia patentu)
result_3 = analizator.analysis_3(lower_bound=2017,upper_bound=2017)
#wykres rozkladu stopni wierzcholkow
#dla sieci patentow
analizator.analysis_4(graph = 0)
#wykres rozkladu stopni wierzcholkow
#dla sieci publikacji
analizator.analysis_4(graph = 1)
#wyznaczenie liczby wierzcholkow,krawedzi oraz sredniego stopnia wierzcholkow
#dla sieci patentow
analizator.analysis_5(graph = 0)
#wyznaczenie liczby wierzcholkow,krawedzi oraz sredniego stopnia wierzcholkow
#dla sieci publikacji
analizator.analysis_5(graph = 1)
#Analiza ilości powiązań w sieci dot. publikacji w zależności od tego czy naukowiec patentuje
result_6 = analizator.analysis_6(sample_size = 2000)
#Analiza ilosci powiązan w sieci dot. publikacji naukowcow patentujacych, w latach poprzedzających zgłoszenie patentu
result_7 = analizator.analysis_7(lower_bound=2019,upper_bound=2018)