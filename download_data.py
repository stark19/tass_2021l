
import requests
import json
import keyboard

class downloader:
    api_url_patents = "https://radon.nauka.gov.pl/opendata/polon/products?resultNumbers=100&"
    api_url_publications = "https://radon.nauka.gov.pl/opendata/polon/publications?resultNumbers=100&"
    starting_year_patents = "publicationDateFrom=2017-01-01"
    starting_year_publications = "yearFrom=2020&yearTo=2020"

    def __init__(self, dump_data=True):
        self.dump_data = dump_data

    def get_patents_data(self):
        api_url = self.api_url_patents + self.starting_year_patents
        all_response = []
        for i in range(51):
            response = requests.get(api_url)
            response_json = response.json()
            for indx, val in enumerate(response_json["results"]):
                all_response.append(response_json["results"][indx])
            api_url = self.api_url_patents + "token=" + response_json["pagination"]["token"]+"&"+self.starting_year_patents
        if self.dump_data:
            self._dump_json(all_response, 'patenty.json')
        else:
            return all_response


    ###### Wcisij "q" aby przerwac pobieranie i zapisac to co udalo sie pobrac
    def get_publications_data(self):
        api_url = self.api_url_publications + self.starting_year_publications
        all_response = []
        temp_dict = dict()
        stop_loop = False

        for i in range(1391):
            if(i%100==0):
                print("Pobrano publikację nr: "+str(i))
            response = requests.get(api_url)
            response_json = response.json()
            for indx, val in enumerate(response_json["results"]):
                temp_dict = {
                    "title": response_json["results"][indx]["authors"],
                    "authors": response_json["results"][indx]["authors"],
                    "year": response_json["results"][indx]["year"]
                }
                if (temp_dict["authors"]):
                    all_response.append(temp_dict)
                ## Kontrola wyjscia z petli
                #if keyboard.is_pressed("q"):
                #    stop_loop = True
                if stop_loop:
                    break
                #######################
            api_url = self.api_url_publications + "token=" + response_json["pagination"]["token"]+"&"+self.starting_year_publications
            #########
            if stop_loop:
                break
            ##########

        if self.dump_data:
            self._dump_json(all_response, 'publikacje_2020.json')
        else:
            return all_response

    def _dump_json(self, all_response, file_name):
        with open(file_name, 'w') as fp:
            json.dump(all_response, fp)
