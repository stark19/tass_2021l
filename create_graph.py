import networkx as nx
import itertools
import json

class generator:
    patents_graph = nx.MultiGraph()
    publications_graph = nx.MultiGraph()
    
    def create_patents_graph(self):
        self.patents_graph.clear()
        with open("patenty.json", 'r') as fp:
            patents = json.load(fp)
        for i in range(len(patents)):
            inventors = list()
            #jeli liczba wynalazcow wynosi 0 lub 1 to nie analizujemy danego patentu
            if(isinstance(patents[i]["inventors"], type(None)) or len(patents[i]["inventors"])==1):
                continue
            for j in range(len(patents[i]["inventors"])):
                inventor_first_name = patents[i]["inventors"][j]["firstName"].lower()
                inventor_last_name = patents[i]["inventors"][j]["lastName"].lower()
                inventor = inventor_first_name + "," + inventor_last_name
                inventors.append(inventor)
            
            inventors_combinations = list(itertools.combinations(inventors, 2))
            
            for j in range(len(inventors_combinations)):
                self.patents_graph.add_edge(inventors_combinations[j][0],inventors_combinations[j][1], year=patents[i]["publicationDate"][:4])
                
        return self.patents_graph
    
    def create_publications_graph(self):
        self.publications_graph.clear()
        #publikacje byly pobierane latami, dlatego mamy w tym miejscu petle
        for k in range(14,22,1):
            filename="publikacje_20"+str(k)+".json"
            print("Opening..."+filename)
            with open(filename, 'r') as fp:
                publications = json.load(fp)
            for i in range(len(publications)):
                if(i%100==0):
                    print("Publikacja nr: "+str(i))
                authors = list()
                #jesli liczba autorow wynosi 0, 1 lub wiecej niz 15 to nie analizujemy danej publikacji
                if(isinstance(publications[i]["authors"], type(None)) or len(publications[i]["authors"])==1 or len(publications[i]["authors"])>=50):
                    continue
                for j in range(len(publications[i]["authors"])):
                    author_first_name = publications[i]["authors"][j]["name"]
                    author_last_name = publications[i]["authors"][j]["lastName"]
                    #jesli w danym polu dot. autora brakuje imienia lub nazwiska to nie bierzemy tego autora pod uwage
                    if(isinstance(author_first_name,type(None)) or isinstance(author_last_name,type(None))):
                        print("Empty author first name or last name\n")
                    else:
                        author_first_name = author_first_name.lower()
                        author_last_name = author_last_name.lower()
                        author = author_first_name + "," + author_last_name
                        #author = author.replace(" ", "")
                    authors.append(author)
                    
                #jesli liczba autorow w danej publikacji jest mniejsza od 2 to nie analizujemy tej publikacji
                if(len(authors)>=2):
                    authors_combinations = list(itertools.combinations(authors, 2))
                    for j in range(len(authors_combinations)):
                        self.publications_graph.add_edge(authors_combinations[j][0],authors_combinations[j][1],year=publications[i]["year"])
        return self.publications_graph
            