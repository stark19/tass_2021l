import networkx as nx
from networkx.exception import NetworkXError
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import random

class analysis:

    def __init__(self, patents = nx.MultiGraph(),publications = nx.MultiGraph()):
        self.patents = patents
        self.publications = publications
    
    def analysis_1(self):
        patents_scientist_list=list(self.patents.nodes())
        result_list = []
        for scientist in patents_scientist_list:
            pub_neighbor_list = []
            pat_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    pub_neighbor_list.append(pub_neighbor)
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pat_neighbor in iter_pat:
                    pat_neighbor_list.append(pat_neighbor)
            
            common_scientist_num = len(set(pub_neighbor_list).intersection(pat_neighbor_list))
            if len(pub_neighbor_list) > 0:
                percent = common_scientist_num/len(pub_neighbor_list)
            else:
                percent = -1
            result_list.append([len(pat_neighbor_list),len(pub_neighbor_list),common_scientist_num, percent])
        self.print_result(result_list, 1)
        return result_list
    
    def analysis_2(self):
        patents_scientist_list=list(self.patents.nodes())
        result_list = []
        for scientist in patents_scientist_list:
            pub_neighbor_list = []
            pat_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    pub_neighbor_list.append(pub_neighbor)
            
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pat_neighbor in iter_pat:
                    pat_neighbor_list.append(pat_neighbor)
            
            common_scientist_list = list(set(pub_neighbor_list).intersection(pat_neighbor_list))
            #count publications for all neighbors
            publications_num_all = 0
            for pub_neighbor in pub_neighbor_list:
                publications_num_all+=len(self.publications.get_edge_data(scientist,pub_neighbor))
            #count publications for common neighbors
            publications_num_common = 0
            for common_scientist in common_scientist_list:
                publications_num_common+=len(self.publications.get_edge_data(scientist,common_scientist))
            
            if publications_num_all>0:
                percent = publications_num_common/publications_num_all
            else:
                percent = -1
            result_list.append([len(pat_neighbor_list),publications_num_all,publications_num_common, percent])
        self.print_result(result_list,2)
        return result_list
    
    def analysis_3(self,lower_bound=2014,upper_bound=2020):
        patents_scientist_list=list(self.patents.nodes())
        result_list = []
        for scientist in patents_scientist_list:
            pub_neighbor_list = []
            pat_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    neighbor_publications = self.publications.get_edge_data(scientist,pub_neighbor)
                    for k in range(len(neighbor_publications)):
                        if int(neighbor_publications[k]['year'])<=upper_bound and int(neighbor_publications[k]['year'])>=lower_bound:
                            pub_neighbor_list.append(pub_neighbor)
                            break
        
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pat_neighbor in iter_pat:
                    neighbor_patents = self.patents.get_edge_data(scientist,pat_neighbor) 
                    for k in range(len(neighbor_patents)):
                        if int(neighbor_patents[k]['year'])==upper_bound:
                            pat_neighbor_list.append(pat_neighbor)
                            break
            
            common_scientist_list = list(set(pub_neighbor_list).intersection(pat_neighbor_list))
            
            #count publications for all neighbors 
            publications_num_all = 0
            for pub_neighbor in pub_neighbor_list:
                all_publications = self.publications.get_edge_data(scientist,pub_neighbor)
                for k in range(len(all_publications)):
                    if int(all_publications[k]['year'])<=upper_bound and int(all_publications[k]['year'])>=lower_bound:
                        publications_num_all+=1
                        
            #count publications for common neighbors
            publications_num_common = 0
            for common_scientist in common_scientist_list:
                all_publications = self.publications.get_edge_data(scientist,common_scientist)
                for k in range(len(all_publications)):
                    if int(all_publications[k]['year'])<=upper_bound and int(all_publications[k]['year'])>=lower_bound:
                        publications_num_common+=1
            
            if publications_num_all>0 and len(pat_neighbor_list)>0:
                percent = publications_num_common/publications_num_all
            else:
                percent = -1
            result_list.append([len(pat_neighbor_list),publications_num_all,publications_num_common, percent])
        self.print_result(result_list,3)
        return result_list
    
    def analysis_4(self,graph=0):
        if graph == 0:
            G = self.patents
        else:
            G = self.publications
        degrees = [(node,val) for (node, val) in G.degree()]
        degrees = sorted(degrees, key=lambda x: x[1], reverse=True)
        c = Counter(elem[1] for elem in degrees)
        plt.figure(figsize=(12,8))
        plt.loglog(c.keys(), c.values(),'bo')
        plt.ylabel('Liczba węzłów')
        plt.xlabel('Stopien węzła')
        if graph == 0:
            plt.title('Rozkład stopni wierzchołków sieci dot. wspólnego patentowania')
        else:
            plt.title('Rozkład stopni wierzchołków sieci dot. wspólnego publikowania')
        plt.savefig('wykres_rozkladu_stopni_wierzcholkow.png')
        plt.show()
        
        
    def analysis_5(self, graph = 0):
        if graph == 0:
            G = self.patents
        else:
            G = self.publications
        print(nx.info(G))
        
    def analysis_6(self,sample_size=1000):
        patents_scientist_list=list(self.patents.nodes())
        patents_scientist_list_num =len(patents_scientist_list)
        result_list_1 = []
        i=0
        while i < sample_size:
            scientist = patents_scientist_list[random.randrange(patents_scientist_list_num)]
            pub_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    pub_neighbor_list.append(pub_neighbor)
        
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                i=i+1
                    
            #count publications for all neighbors 
            publications_num_all = 0
            for pub_neighbor in pub_neighbor_list:
                publications_num_all+=len(self.publications.get_edge_data(scientist,pub_neighbor))
            
            result_list_1.append(publications_num_all)
            
        publications_scientist_list=list(self.publications.nodes())
        publications_scientist_list_num =len(publications_scientist_list)
        result_list_2 = []
        i=0
        while i < sample_size:
            scientist = publications_scientist_list[random.randrange(publications_scientist_list_num)]
            pub_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    pub_neighbor_list.append(pub_neighbor)
        
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                i=i+1
            else:
                continue
                    
            #count publications for all neighbors 
            publications_num_all = 0
            for pub_neighbor in pub_neighbor_list:
                publications_num_all+=len(self.publications.get_edge_data(scientist,pub_neighbor))
            
            result_list_2.append(publications_num_all)
        buckets_1 = [0,0,0,0,0]
        buckets_2 = [0,0,0,0,0]
        for result in result_list_1:
                if result>=0 and result<5:
                    buckets_1[0]+=1
                elif result>=5 and result<10:
                    buckets_1[1]+=1
                elif result>=10 and result<20:
                    buckets_1[2]+=1
                elif result>=20 and result<40:
                    buckets_1[3]+=1
                elif result>=40:
                    buckets_1[4]+=1

        for result in result_list_2:
                if result>=0 and result<5:
                    buckets_2[0]+=1
                elif result>=5 and result<10:
                    buckets_2[1]+=1
                elif result>=10 and result<20:
                    buckets_2[2]+=1
                elif result>=20 and result<40:
                    buckets_2[3]+=1
                elif result>=40:
                    buckets_2[4]+=1
                    
        print("Srednia liczba powiązań w sieci publikacji - naukowcy patentujący= "+str(sum(result_list_1)/sample_size))
        print("Srednia liczba powiązań w sieci publikacji - naukowcy niepatentujący= "+str(sum(result_list_2)/sample_size))            
        x_label = ['0-5','5-10', '10-20', '20-40', '>40']
        plt.figure(figsize=(12,8))
        plt.bar(x_label,buckets_1)
        plt.ylabel('Liczba naukowców')
        plt.xlabel('Liczba powiązań w sieci publikacji')
        plt.title('Wykres liczby powiązań w sieci publikacji - naukowcy patentujący')
        plt.show()
        
        plt.figure(figsize=(12,8))
        plt.bar(x_label,buckets_2)
        plt.ylabel('Liczba naukowców')
        plt.xlabel('Liczba powiązań w sieci publikacji')
        plt.title('Wykres liczby powiązań w sieci publikacji - naukowcy niepatentujący')
        plt.show()
        
        result_list = [result_list_1,result_list_2]
        return result_list        
        
    def analysis_7(self,lower_bound=2014,upper_bound=2020):
        patents_scientist_list=list(self.patents.nodes())
        result_list = []
        for scientist in patents_scientist_list:
            pub_neighbor_list = []
            pat_neighbor_list = []
            try:
                iter_pub = self.publications.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pub_neighbor in iter_pub:
                    neighbor_publications = self.publications.get_edge_data(scientist,pub_neighbor)
                    for k in range(len(neighbor_publications)):
                        if int(neighbor_publications[k]['year'])==lower_bound:
                            pub_neighbor_list.append(pub_neighbor)
                            break
        
            try:
                iter_pat = self.patents.neighbors(scientist)
            except NetworkXError:
                continue
            else:
                for pat_neighbor in iter_pat:
                    neighbor_patents = self.patents.get_edge_data(scientist,pat_neighbor) 
                    for k in range(len(neighbor_patents)):
                        if int(neighbor_patents[k]['year'])==upper_bound:
                            pat_neighbor_list.append(pat_neighbor)
                            break
                        
            if len(pub_neighbor_list)==0:
                continue
            
            #count publications for all neighbors 
            publications_num_all = 0
            for pub_neighbor in pub_neighbor_list:
                all_publications = self.publications.get_edge_data(scientist,pub_neighbor)
                for k in range(len(all_publications)):
                    if int(all_publications[k]['year'])==lower_bound:
                        publications_num_all+=1        
            result_list.append(publications_num_all)
            
        buckets_1 = [0,0,0,0,0]
        for result in result_list:
                if result>=0 and result<5:
                    buckets_1[0]+=1
                elif result>=5 and result<10:
                    buckets_1[1]+=1
                elif result>=10 and result<20:
                    buckets_1[2]+=1
                elif result>=20 and result<40:
                    buckets_1[3]+=1
                elif result>=40:
                    buckets_1[4]+=1
                    
        for i in range(5):
            buckets_1[i]=100*buckets_1[i]/len(result_list)
                    
        x_label = ['0-5','5-10', '10-20', '20-40', '>40']
        plt.figure(figsize=(12,8))
        plt.bar(x_label,buckets_1)
        plt.ylabel('Procent naukowców[%]')
        plt.xlabel('Liczba powiązań w sieci publikacji')
        plt.title('Wykres liczby powiązań w sieci publikacji - naukowcy patentujący w roku 2018 - publikacje 2019')
        plt.show()
            
        return result_list
        
        
    def print_result(self,result_list, analysis_num = 1):
        mylinspace = np.linspace(start=0,stop=1,num=11)
        counters = np.zeros(10)
        counter_0 = np.zeros(1)
        global_counter = 0
        for result in result_list:
            for j in range(10):
                if result[3]>mylinspace[j] and result[3]<=mylinspace[j+1]:
                    counters[j]+=1
                    global_counter+=1
                    break
        for result in result_list:
            if result[3]==0:
                counter_0[0]+=1
                global_counter+=1
                
        counters=np.concatenate((counter_0,counters))
        counters_percent =100* counters/global_counter
        plt.figure(figsize=(12,8))
        x_label = ['0%','0-10%', '10-20%', '20-30%', '30-40%', '40-50%','50-60%','60-70%','70-80%','80-90%','90-100%']
        plt.bar(x_label,counters)
        plt.ylabel('Liczba naukowców')
        plt.xlabel('Procent powiązań danego naukowca w sieci dot. publikacji, które ma ze współtwórcami swoich patentów')
        #plt.xlabel('Procent jaki stanowią sąsiedzi naukowca z sieci dot.patentów w zbiorze sąsiadów danego naukowca w sieci dot.publikacji')
        plt.title('Wykres powiazań: analiza: '+str(analysis_num))
        #plt.savefig('Wykres powiazań.png')
        plt.show()
        
        plt.figure(figsize=(12,8))
        plt.bar(x_label,counters_percent)
        plt.ylabel('Procent naukowców[%]')
        plt.xlabel('Procent powiązań danego naukowca w sieci dot. publikacji, które ma ze współtwórcami swoich patentów')
        plt.title('Wykres powiazań: analiza: '+str(analysis_num))
        #plt.savefig('Wykres powiazań.png')
        plt.show()
        
        